variable "gcp_project_id" {}
variable "gitlab_token" {}
variable "gitlab_project_id" {}
variable "gitlab_url" {
  default = "https://gitlab.com/"
}
variable "zone" {
  default = "us-central1-a"
}