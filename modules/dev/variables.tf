variable "fleeting_provider" {
  description = "The system which providers infrastructure for the Fleeting Runners"
}

variable "fleeting_os" {
  description = "The operating system for the Fleeting Runners"
}